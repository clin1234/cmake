# Distributed under the OSI-approved BSD 3-Clause License.  See accompanying
# file Copyright.txt or https://cmake.org/licensing for details.

# This module is shared by multiple languages; use include blocker.
if(__WINDOWS_POCC)
  return()
endif()
set(__WINDOWS_POCC 1)

if(CMAKE_VERBOSE_MAKEFILE)
  set(CMAKE_CL_NOLOGO)
else()
  set(CMAKE_CL_NOLOGO "/nologo")
endif()

set(CMAKE_BUILD_TYPE_INIT Debug)

set(CMAKE_CREATE_WIN32_EXE "/subsystem:windows")
set(CMAKE_CREATE_CONSOLE_EXE "/subsystem:console")
set(_PLATFORM_LINK_FLAGS "")

set (CMAKE_BUILD_TYPE Debug CACHE STRING
     "Choose the type of build, options are: Debug Release RelWithDebInfo MinSizeRel.")

set(CMAKE_SUPPORT_WINDOWS_EXPORT_ALL_SYMBOLS 1)
if(NOT CMAKE_NO_BUILD_TYPE AND CMAKE_GENERATOR MATCHES "Pelles IDE")
  set (CMAKE_NO_BUILD_TYPE 1)
endif()

if("${CMAKE_GENERATOR}" MATCHES "Pelles IDE")
  set(POIDE 1)
else()
  set(POIDE 0)
endif()

if(NOT POCC_VERSION)
  if("x${CMAKE_C_COMPILER_ID}" STREQUAL "xPOCC")
    set(_compiler_version ${CMAKE_C_COMPILER_VERSION})
  elseif(CMAKE_C_COMPILER_VERSION)
    set(_compiler_version ${CMAKE_C_COMPILER_VERSION})
  endif()
endif()

set(_PLATFORM_DEFINES "/DWIN32")
set(CMAKE_C_STANDARD_LIBRARIES_INIT "kernel32.lib user32.lib gdi32.lib winspool.lib shell32.lib ole32.lib oleaut32.lib uuid.lib comdlg32.lib advapi32.lib")

if(POCC_C_ARCHITECTURE_ID)
  set(_MACHINE_ARCH_FLAG "/machine:${MSVC_C_ARCHITECTURE_ID}")

foreach(t EXE SHARED MODULE)
  string(APPEND CMAKE_${t}_LINKER_FLAGS_INIT " ${_MACHINE_ARCH_FLAG}")
  string(APPEND CMAKE_${t}_LINKER_FLAGS_DEBUG_INIT " /debug /debugtype:both")
  string(APPEND CMAKE_${t}_LINKER_FLAGS_RELWITHDEBINFO_INIT " /debug /debug:type both")
  # for release and minsize release default to no incremental linking
  string(APPEND CMAKE_${t}_LINKER_FLAGS_MINSIZEREL_INIT " ")
  string(APPEND CMAKE_${t}_LINKER_FLAGS_RELEASE_INIT " ")
endforeach()

  if(NOT MSVC_VERSION LESS 1400)
    # for 2005 make sure the manifest is put in the dll with mt
    set(_CMAKE_VS_LINK_DLL "<CMAKE_COMMAND> -E vs_link_dll --intdir=<OBJECT_DIR> --rc=<CMAKE_RC_COMPILER> --mt=<CMAKE_MT> --manifests <MANIFESTS> -- ")
    set(_CMAKE_VS_LINK_EXE "<CMAKE_COMMAND> -E vs_link_exe --intdir=<OBJECT_DIR> --rc=<CMAKE_RC_COMPILER> --mt=<CMAKE_MT> --manifests <MANIFESTS> -- ")
  endif()
  set(CMAKE_C_CREATE_SHARED_LIBRARY
    "${_CMAKE_VS_LINK_DLL}<CMAKE_LINKER> ${CMAKE_CL_NOLOGO} <OBJECTS> ${CMAKE_START_TEMP_FILE} /out:<TARGET> /implib:<TARGET_IMPLIB> /pdb:<TARGET_PDB> /dll /version:<TARGET_VERSION_MAJOR>.<TARGET_VERSION_MINOR>${_PLATFORM_LINK_FLAGS} <LINK_FLAGS> <LINK_LIBRARIES> ${CMAKE_END_TEMP_FILE}")

  set(CMAKE_C_CREATE_SHARED_MODULE ${CMAKE_C_CREATE_SHARED_LIBRARY})
  set(CMAKE_C_CREATE_STATIC_LIBRARY  "<CMAKE_AR> ${CMAKE_CL_NOLOGO} <LINK_FLAGS> /out:<TARGET> <OBJECTS> ")

  set(CMAKE_C_COMPILE_OBJECT
    "<CMAKE_C_COMPILER> ${CMAKE_START_TEMP_FILE} ${CMAKE_CL_NOLOGO}${_COMPILE_C} <DEFINES> <INCLUDES> <FLAGS> /Fo<OBJECT> /Fd<TARGET_COMPILE_PDB>${_FS_C} -c <SOURCE>${CMAKE_END_TEMP_FILE}")
  set(CMAKE_C_CREATE_PREPROCESSED_SOURCE
    "<CMAKE_C_COMPILER> > <PREPROCESSED_SOURCE> ${CMAKE_START_TEMP_FILE} ${CMAKE_CL_NOLOGO}${_COMPILE_C} <DEFINES> <INCLUDES> <FLAGS> -E <SOURCE>${CMAKE_END_TEMP_FILE}")
  set(CMAKE_C_CREATE_ASSEMBLY_SOURCE
    "<CMAKE_C_COMPILER> ${CMAKE_START_TEMP_FILE} ${CMAKE_CL_NOLOGO}${_COMPILE_C} <DEFINES> <INCLUDES> <FLAGS> /FoNUL /FAs /Fa<ASSEMBLY_SOURCE> /c <SOURCE>${CMAKE_END_TEMP_FILE}")

  set(CMAKE_C_USE_RESPONSE_FILE_FOR_OBJECTS 1)
  set(CMAKE_C_LINK_EXECUTABLE
    "${_CMAKE_VS_LINK_EXE}<CMAKE_LINKER> ${CMAKE_CL_NOLOGO} <OBJECTS> ${CMAKE_START_TEMP_FILE} /out:<TARGET> /implib:<TARGET_IMPLIB> /pdb:<TARGET_PDB> /version:<TARGET_VERSION_MAJOR>.<TARGET_VERSION_MINOR>${_PLATFORM_LINK_FLAGS} <CMAKE_C_LINK_FLAGS> <LINK_FLAGS> <LINK_LIBRARIES>${CMAKE_END_TEMP_FILE}")

  set(CMAKE_PCH_EXTENSION .pch)
  set(CMAKE_LINK_PCH ON)

  if (NOT ${CMAKE_C_COMPILER_ID} STREQUAL "Clang")
    set(CMAKE_PCH_COPY_COMPILE_PDB ON)
  endif()
  set(CMAKE_C_COMPILE_OPTIONS_USE_PCH /Yu<PCH_HEADER> /Fp<PCH_FILE> /FI<PCH_HEADER>)
  set(CMAKE_C_COMPILE_OPTIONS_CREATE_PCH /Yc<PCH_HEADER> /Fp<PCH_FILE> /FI<PCH_HEADER>)

  if("x${CMAKE_C_COMPILER_ID}" STREQUAL "xMSVC")
    set(_CMAKE_C_IPO_SUPPORTED_BY_CMAKE YES)
    set(_CMAKE_C_IPO_MAY_BE_SUPPORTED_BY_COMPILER YES)

    set(CMAKE_C_COMPILE_OPTIONS_IPO "/GL")
    set(CMAKE_C_LINK_OPTIONS_IPO "/INCREMENTAL:NO" "/LTCG")
    string(REPLACE "<LINK_FLAGS> " "/LTCG <LINK_FLAGS> "
      CMAKE_C_CREATE_STATIC_LIBRARY_IPO "${CMAKE_C_CREATE_STATIC_LIBRARY}")
  endif()

    if(CMAKE_MSVC_RUNTIME_LIBRARY_DEFAULT)
      set(_MD "")
    else()
      set(_MD " /MD")
    endif()

      string(APPEND CMAKE_C_FLAGS_INIT " ${_PLATFORM_DEFINES}${_PLATFORM_DEFINES_C} /D_WINDOWS${_W3}${_FLAGS_C}")
      string(APPEND CMAKE_C_FLAGS_DEBUG_INIT "${_MDd} /Zi /Ob0 ${_RTC1}")
      string(APPEND CMAKE_C_FLAGS_RELEASE_INIT "${_MD} /O2 /Ob2 /DNDEBUG")
      string(APPEND CMAKE_C_FLAGS_RELWITHDEBINFO_INIT "${_MD} /Zi /O2 /Ob1 /DNDEBUG")
      string(APPEND CMAKE_C_FLAGS_MINSIZEREL_INIT "${_MD} /O1 /Ob1 /DNDEBUG")
    unset(_Wall)
    unset(_W3)
    unset(_MD)

    set(CMAKE_C_COMPILE_OPTIONS_PELLES_RUNTIME_LIBRARY_MultiThreaded         -MT)
    set(CMAKE_C_COMPILE_OPTIONS_PELLES_RUNTIME_LIBRARY_MultiThreadedDLL      -MD)
  endif()
  set(CMAKE_C_LINKER_SUPPORTS_PDB ON)

  __windows_compiler_pocc_enable_rc("${_PLATFORM_DEFINES} ${_PLATFORM_DEFINES_C}")

  # define generic information about compiler dependencies
  if (MSVC_VERSION GREATER 1300)
    set(CMAKE_DEPFILE_FLAGS_C "/showIncludes")
    set(CMAKE_C_DEPFILE_FORMAT msvc)
  endif()

macro(__windows_compiler_pocc_enable_rc flags)
  if(NOT CMAKE_RC_COMPILER_INIT)
    set(CMAKE_RC_COMPILER_INIT rc)
  endif()
  if(NOT CMAKE_RC_FLAGS_INIT)
    # llvm-rc fails when flags are specified with /D and no space after
    string(REPLACE " /D" " -D" fixed_flags " ${flags}")
    string(APPEND CMAKE_RC_FLAGS_INIT " ${fixed_flags}")
  endif()
  if(NOT CMAKE_RC_FLAGS_DEBUG_INIT)
    string(APPEND CMAKE_RC_FLAGS_DEBUG_INIT " -D_DEBUG")
  endif()

  enable_language(RC)
  if(NOT DEFINED CMAKE_NINJA_CMCLDEPS_RC)
    set(CMAKE_NINJA_CMCLDEPS_RC 1)
  endif()
endmacro()
