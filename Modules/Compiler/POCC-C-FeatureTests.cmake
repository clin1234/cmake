
set(_cmake_oldestSupported "__POCC__ >= 250")

set(_cmake_feature_test_c_)
set(POCC_C11 "${_cmake_oldestSupported} && defined(__STDC_VERSION__) && __STDC_VERSION__ >= 201112L")
set(_cmake_feature_test_c_static_assert "${POCC_C11}")
set(POCC_C99 "${_cmake_oldestSupported} && defined(__STDC_VERSION__) && __STDC_VERSION__ >= 199901L")
set(_cmake_feature_test_c_restrict "${POCC_C99}")
set(_cmake_feature_test_c_variadic_macros "${POCC_C99}")
