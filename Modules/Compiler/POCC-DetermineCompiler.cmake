
set(_compiler_id_pp_test "defined(__POCC__)")

# Minor version == 00 since Pelles C 6.00.
set(_compiler_id_version_compute "
#define @PREFIX@COMPILER_VERSION_MAJOR @MACRO_DEC@(__POCC__ / 100)
#define @PREFIX@COMPILER_VERSION_MINOR @MACRO_DEC@(_MSC_VER % 100)
)
