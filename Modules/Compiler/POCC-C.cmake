include(Compiler / POCC) _compiler_pocc(C)

string(APPEND CMAKE_C_FLAGS_MINSIZEREL_INIT " -DNDEBUG")
string(APPEND CMAKE_C_FLAGS_RELEASE_INIT" -DNDEBUG")
string(APPEND CMAKE_C_FLAGS_RELWITHDEBINFO_INIT " -DNDEBUG")

if (CMAKE_C_COMPILER_VERSION VERSION_GREATER_EQUAL 9.00)
#[[
Default version is C17, which is C11 except:
* Remove ATOMIC_VAR_INIT
* ENhancements to atomic threading functions
]]
  set(CMAKE_C17_STANDARD_COMPILE_OPTION "/std:c17")
  __compiler_check_default_language_standard(C 7.00 11 9.00 17)
endif()
if (CMAKE_C_COMPILER_VERSION VERSION_GREATER_EQUAL 7.00)
    set(CMAKE_C11_STANDARD_COMPILE_OPTION "/std:c11")
    set(CMAKE_C11_STANDARD__HAS_FULL_SUPPORT ON)
  __compiler_check_default_language_standard(C 2.50 99 7.00 11)
endif()

set(CMAKE_C99_STANDARD__HAS_FULL_SUPPORT ON)
set(CMAKE_C99_STANDARD_COMPILE_OPTION "/std:c99")
