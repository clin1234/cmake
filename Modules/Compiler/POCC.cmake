# This module is shared by multiple languages; use include blocker.
if(__COMPILER_POCC)
  return()
endif()
set(__COMPILER_POCC 1)

include(Compiler/CMakeCommonCompilerMacros)

macro(_compiler_pocc lang)
  set(CMAKE_C_VERBOSE_FLAG "/V1")

  string(APPEND CMAKE_C_FLAGS_INIT " ")
  string(APPEND CMAKE_C_FLAGS_DEBUG_INIT " /Zi")
  string(APPEND CMAKE_C_FLAGS_MINSIZEREL_INIT " /Os /Ox")
  string(APPEND CMAKE_C_FLAGS_RELEASE_INIT " /Ot /Ox")
  string(APPEND CMAKE_C_FLAGS_RELWITHDEBINFO_INIT " /Zd /Ot /Ox")

  # C create import library
set(CMAKE_C_CREATE_IMPORT_LIBRARY
  "polib <TARGET_IMPLIB> /out:<TARGET_QUOTED>")

# C link a object files into an executable file
set(CMAKE_C_LINK_EXECUTABLE
  "polink name <TARGET> <LINK_FLAGS> file {<OBJECTS>} <LINK_LIBRARIES>")

# C compile a file into an object file
set(CMAKE_C_COMPILE_OBJECT
  "<CMAKE_C_COMPILER> <DEFINES> <INCLUDES> <FLAGS> /Fo <OBJECT> <SOURCE>")
# C preprocess a source file
set(CMAKE_C_CREATE_PREPROCESSED_SOURCE
  "<CMAKE_C_COMPILER> <DEFINES> <INCLUDES> <FLAGS> /Fo <PREPROCESSED_SOURCE> /P <SOURCE>")
# C output to assembly
if(CMAKE_HOST_SYSTEM_PROCESSOR STREQUAL "AMD64")
set(CMAKE_C_CREATE_ASSEMBLY_SOURCE "<CMAKE_${lang}_COMPILER> <DEFINES> <INCLUDES> <FLAGS> <SOURCE> /Tx64-asm
  /Fo <ASSEMBLY_SOURCE>")
else()
set(CMAKE_C_CREATE_ASSEMBLY_SOURCE "<CMAKE_${lang}_COMPILER> <DEFINES> <INCLUDES> <FLAGS> <SOURCE> /Tx86-asm
  /Fo <ASSEMBLY_SOURCE>")
endif()
# C create a shared library
set(CMAKE_C_CREATE_SHARED_LIBRARY
  "polink /DLL <TARGET> <LINK_FLAGS> implib=<TARGET_IMPLIB> file {<OBJECTS>} <LINK_LIBRARIES>")

# C create a shared module
set(CMAKE_C_CREATE_SHARED_MODULE
  "wlink  name <TARGET> <LINK_FLAGS> file {<OBJECTS>} <LINK_LIBRARIES>")

# C create a static library
set(CMAKE_C_CREATE_STATIC_LIBRARY
  "wlib  -c -n -b <TARGET_QUOTED> <LINK_FLAGS> <OBJECTS> ")
endmacro()
